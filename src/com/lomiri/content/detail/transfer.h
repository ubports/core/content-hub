/*
 * Copyright © 2013 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Thomas Voß <thomas.voss@canonical.com>
 */
#ifndef TRANSFER_H_
#define TRANSFER_H_

#include <QDir>
#include <QObject>
#include <QStringList>
#include <QtDBus/QDBusMessage>
#include <QtDBus/QDBusContext>
#include <lomiri/download_manager/error.h>
#include <com/lomiri/content/scope.h>

namespace com
{
namespace lomiri
{
namespace content
{
namespace detail
{
class Transfer : public QObject, protected QDBusContext
{
    Q_OBJECT

  public:
    Transfer(const int, const QString&, const QString&, const int, const QString&, QObject* parent = nullptr);
    Transfer(const Transfer&) = delete;
    virtual ~Transfer();

    Transfer& operator=(const Transfer&) = delete;

    void SetSourceStartedByContentHub(bool started);
    bool WasSourceStartedByContentHub() const;
    void SetShouldBeStartedByContentHub(bool start);
    bool ShouldBeStartedByContentHub() const;

Q_SIGNALS:
    void StateChanged(int State);
    void StoreChanged();
    void SelectionTypeChanged(int SelectionType);
    void DownloadIdChanged(QString DownloadId);
    void DownloadManagerError(QString ErrorMessage);

  public Q_SLOTS:
    int State();
    void Start();
    void Handled();
    void Charge(const QVariantList&);
    QVariantList Collect();
    void Abort();
    void Finalize();
    int Store(QString &, QString &);
    QString SetStore(int, const QString &);
    int SelectionType();
    void SetSelectionType(int);
    int Id();
    int Direction();
    QString source();
    QString destination();
    QString export_path();
    QString import_path();
    QString DownloadId();
    void SetDownloadId(QString DownloadId);
    void DownloadComplete(QString destFilePath);
    void Download();
    void DownloadError(Lomiri::DownloadManager::Error* error);
    QString ContentType();
    void AddItemsFromDir(QDir dir);

  private:
    struct Private;
    QScopedPointer<Private> d;

    QString calculateStorePath(Scope, const QString &);

    friend class Service;
    void setStoreInternal(Scope, const QString &, const QString &);

};
}
}
}
}

#endif // TRANSFER_H_
