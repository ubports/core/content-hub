/* Copied from the original content-hub-glib.h pre-DBus interface change */

#ifndef __GLIB_CONTENT_HUB_GLIB_COMPAT_H__
#define __GLIB_CONTENT_HUB_GLIB_COMPAT_H__

#include <gio/gio.h>

G_BEGIN_DECLS

typedef struct _ContentHubTransfer ContentHubTransfer;

G_GNUC_DEPRECATED_FOR("Use content_hub_transfer_call_store_with_scope_and_type()")
void content_hub_transfer_call_store (
    ContentHubTransfer *proxy,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

G_GNUC_DEPRECATED_FOR("Use content_hub_transfer_call_store_with_scope_and_type()")
gboolean content_hub_transfer_call_store_finish (
    ContentHubTransfer *proxy,
    gchar **out_uri,
    GAsyncResult *res,
    GError **error);

G_GNUC_DEPRECATED_FOR("Use content_hub_transfer_call_store_with_scope_and_type_sync()")
gboolean content_hub_transfer_call_store_sync (
    ContentHubTransfer *proxy,
    gchar **out_uri,
    GCancellable *cancellable,
    GError **error);

G_GNUC_DEPRECATED_FOR("Use content_hub_transfer_call_set_store_by_scope_and_type()")
void content_hub_transfer_call_set_store (
    ContentHubTransfer *proxy,
    const gchar *arg_uri,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);

G_GNUC_DEPRECATED_FOR("Use content_hub_transfer_call_set_store_by_scope_and_type()")
gboolean content_hub_transfer_call_set_store_finish (
    ContentHubTransfer *proxy,
    GAsyncResult *res,
    GError **error);

G_GNUC_DEPRECATED_FOR("Use content_hub_transfer_call_set_store_by_scope_and_type_sync()")
gboolean content_hub_transfer_call_set_store_sync (
    ContentHubTransfer *proxy,
    const gchar *arg_uri,
    GCancellable *cancellable,
    GError **error);

G_GNUC_DEPRECATED_FOR("Why do you call this function?")
void content_hub_transfer_complete_set_store (
    ContentHubTransfer *object,
    GDBusMethodInvocation *invocation);

G_GNUC_DEPRECATED_FOR("Why do you call this function?")
void content_hub_transfer_complete_store (
    ContentHubTransfer *object,
    GDBusMethodInvocation *invocation,
    const gchar *uri);

G_END_DECLS

#endif
