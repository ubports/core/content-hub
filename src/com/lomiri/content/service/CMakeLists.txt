# Copyright © 2013 Canonical Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Authored by: Thomas Voss <thomas.voss@canonical.com>

add_definitions(-DQT_NO_KEYWORDS)

if(LIBERTINE_FOUND)
   add_definitions(-DWITH_LIBERTINE)
endif()

include_directories(${CMAKE_CURRENT_BINARY_DIR})

qt5_add_dbus_adaptor(
  CONTENT_SERVICE_SKELETON ${CMAKE_SOURCE_DIR}/src/com/lomiri/content/detail/com.lomiri.content.Service.xml
  detail/service.h com::lomiri::content::detail::Service)

include_directories(
  ${CMAKE_SOURCE_DIR}/src
  ${CMAKE_SOURCE_DIR}/src/com/lomiri/content
  ${GIO_INCLUDE_DIRS}
  ${GSETTINGS_INCLUDE_DIRS}
  ${LOMIRI_LAUNCH_INCLUDE_DIRS}
  ${LIBERTINE_INCLUDE_DIRS}
)

add_executable(
  lomiri-content-hub-service

  main.cpp
  registry.cpp
  registry-updater.cpp
  ../debug.cpp
  ../utils.cpp
  ${CONTENT_SERVICE_SKELETON}
)

set_target_properties(
  lomiri-content-hub-service
  PROPERTIES
  AUTOMOC TRUE
)

target_link_libraries(lomiri-content-hub-service Qt5::Core Qt5::DBus Qt5::Gui)

target_link_libraries(
  lomiri-content-hub-service
 
  lomiri-content-hub
  ${GIO_LDFLAGS}
  ${GSETTINGS_LDFLAGS}
  ${LOMIRI_LAUNCH_LDFLAGS}
  ${LIBERTINE_LDFLAGS}
)

install(
  TARGETS lomiri-content-hub-service
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

configure_file(
  com.lomiri.content.dbus.Service.service.in
  com.lomiri.content.dbus.Service.service
  @ONLY)

install(
  FILES ${CMAKE_CURRENT_BINARY_DIR}/com.lomiri.content.dbus.Service.service
  DESTINATION ${CMAKE_INSTALL_DATADIR}/dbus-1/services
)

###########################
# GSettings
###########################

add_schema ("com.lomiri.content.hub.gschema.xml")

install(
  FILES xorg.png
  DESTINATION ${CMAKE_INSTALL_DATADIR}/lomiri-content-hub/icons/
)
