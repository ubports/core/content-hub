# Italian translation for content-hub
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the content-hub package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: content-hub\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-04 07:08+0000\n"
"PO-Revision-Date: 2023-04-02 03:45+0000\n"
"Last-Translator: Sylke Vicious <silkevicious@tuta.io>\n"
"Language-Team: Italian <https://hosted.weblate.org/projects/lomiri/content-"
"hub/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.17-dev\n"
"X-Launchpad-Export-Date: 2016-12-01 04:56+0000\n"

#: src/com/lomiri/content/detail/service.cpp:245
msgid "Download Complete"
msgstr "Scaricamento completato"

#: src/com/lomiri/content/detail/service.cpp:263
msgid "Open"
msgstr "Apri"

#: src/com/lomiri/content/detail/service.cpp:270
msgid "Dismiss"
msgstr "Chiudi"

#: src/com/lomiri/content/detail/service.cpp:288
msgid "Download Failed"
msgstr "Scaricamento non riuscito"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Choose from"
msgstr "Scegli da"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Open with"
msgstr "Apri con"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Share to"
msgstr "Condividi con"

#: import/Lomiri/Content/ContentPeerPicker10.qml:162
#: import/Lomiri/Content/ContentPeerPicker13.qml:170
#: import/Lomiri/Content/ContentPeerPicker11.qml:180
msgid "Apps"
msgstr "App"

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can provide this type "
"of content."
msgstr ""
"Non ci sono app installate che possano fornire questo tipo di contenuto."

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can handle this type "
"of content."
msgstr ""
"Non ci sono app installate che possano gestire questo tipo di contenuto."

#: import/Lomiri/Content/ContentPeerPicker10.qml:214
#: import/Lomiri/Content/ContentPeerPicker13.qml:223
#: import/Lomiri/Content/ContentPeerPicker11.qml:232
msgid "Devices"
msgstr "Dispositivi"

#: import/Lomiri/Content/ContentPeerPicker10.qml:251
#: import/Lomiri/Content/ContentPeerPicker11.qml:59
#: import/Lomiri/Content/ContentTransferHint.qml:65
msgid "Cancel"
msgstr "Annulla"

#: import/Lomiri/Content/ContentTransferHint.qml:52
msgid "Transfer in progress"
msgstr "Trasferimento in corso"

#: examples/picker-qml/picker.qml:21
msgid "Peer Picker Example"
msgstr "Esempio di Selezionatore di Peer"

#: examples/picker-qml/picker.qml:37
msgid "Sources"
msgstr "Sorgenti"

#: examples/picker-qml/picker.qml:39
msgid "Select source"
msgstr "Seleziona sorgente"

#: examples/picker-qml/picker.qml:52
msgid "Results"
msgstr "Risultati"
